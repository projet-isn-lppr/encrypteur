/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package projet.isn;

import image.Picture;
import gui.Dialog;
import javax.swing.JOptionPane;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
/**
 *
 * @author PC
 */
public class Coder {
    private static int offset;
    
    /* 
     * Fonction principale
     * mode = true : encryptage; mode = false : décryptage
     */
    public static void main(String text, boolean mode) {
        if (mode)
            encoding(text);
        else
            decoding();
    }
    
    //Encryptage du message
    private static void encoding(String text) {
        boolean ok = true;
        int res = 0;
        int[] dim = null;
        String offsetBin = "", path = "";
        String[] binArray;
        Picture pic = null;
        
        log("log.txt", "\nEncryptage\n");
        do {
            ok = true;
            log("log.txt", "Openning dialog box...");
            Dialog dialog = new Dialog(new javax.swing.JFrame(), true, true);
            
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
            
            //Récupération de l'image
            path = dialog.getPath();
            
            pic = new Picture(path);
            dim = pic.getDimensions();
            res = dim[0] * dim [1];       //Largeur, hauteur
            
            if (text.length() * 16 > res || res < 8) {
                JOptionPane.showMessageDialog(null, "Image trop petite", "Erreur: taille", JOptionPane.INFORMATION_MESSAGE);
                log("log.txt", "/!\\Error: Image too little!");
                ok = false;
            }
        } while(!ok);
        log("log.txt", "Path: " + path + "\n");
        
        if (!path.equals("")) {
            imageModifier(path);
            
            /* 
             * <==!Expliqué par Franck
             */
            log("log.txt", "Offset (d\u00e9cimal): " + offset + "\nMessage: " + text + "\n");
            //Code César
            text = cesar(text, true);
            log("log.txt", "Offset (d\u00e9cimal): " + offset + "\nMessage d\u00e9cal\u00e9: " + text + "\n");
            
            binArray = new String[text.length()];
            offsetBin = dTob(offset, 8);
            log("log.txt", "Offset (binaire): " + offsetBin + "\nCaractères (d\u00e9cimal/binaire):\n");
            /* 
             * ==>
             */
            
            /* 
             * <==!Expliqué par Anthony
             * Conversion caractère -> décimal -> binaire
             */
            for(int count = 0; count < text.length(); count++) {
                binArray[count] = dTob(unicodeDeci(text.charAt(count)), 16);
                log("log.txt", unicodeDeci(text.charAt(count)) + "/" + binArray[count] + "\n");
            }
            /* 
             * ==>
             */
            
            /* 
             * <==!Expliqué par Damien
             * Écriture de l'offset sur le rouge
             */
            int x = 0, y = 0, count = 0;
            log("log.txt", "Writing offset...\n");
            while(count < 8) {
                String redBin = dTob(pic.getPixelColor(x, y, "red"), 8);
                String temp = "";
                
                for(int count2 = 0; count2 < 7; count2++)
                    temp += redBin.charAt(count2);
                temp += offsetBin.charAt(count);
                
                Color col = new Color(bTod(temp), pic.getPixelColor(x, y, "green"), pic.getPixelColor(x, y, "blue"), pic.getPixelColor(x, y, "alpha"));
                pic.draw(col.getRGB(), x, y);
                
                ++x;
                if (x == dim[0]) {
                    x = 0;
                    ++y;
                }
                count++;
            }
            
            /* 
             * Écriture du message sur le vert et de la balise sur le bleu
             * La balise indique si une information est inscrite dans l'octet lu
             * Balise = 1 -> lire le LSB du vert; Balise = 0 -> arrêter la lecture
             */
            log("log.txt", "Writing message...\n");
            count = (x = (y = 0));
            for(int count2 = 0; count2 < text.length(); count2++) {
                count = 0;
                while(count < 16) {
                    String greenBin = dTob(pic.getPixelColor(x, y, "green"), 8);
                    String blueBin = dTob(pic.getPixelColor(x, y, "blue"), 8);
                    String temp1 = "", temp2 = "";
                    
                    for(int count3 = 0; count3 < 7; count3++) {
                        temp1 += greenBin.charAt(count3);
                        temp2 += blueBin.charAt(count3);
                    }
                    temp1 += binArray[count2].charAt(count);
                    temp2 += "1";
                    
                    Color col = new Color(pic.getPixelColor(x, y, "red"), bTod(temp1), bTod(temp2), pic.getPixelColor(x, y, "alpha"));
                    pic.draw(col.getRGB(), x, y);
                    
                    ++x;
                    if (x == dim[0]) {
                        x = 0;
                        ++y;
                    }
                    count++;
                }
            }
            
            /* 
             * Mise à 0 de la balise des pixels restants
             */
            log("log.txt", "Completing picture...\n");
            count = (text.length() * 16) - 1;
            if (x > 0)
                --x;
            if (y > 0 && x == 0) {
                x = dim[0] - 1;
                --y;
            }
            while(count < res) {
                log("log.txt", "x = " + x + "; y = " + y + "\n");
                String blueBin = dTob(pic.getPixelColor(x, y, "blue"), 8);
                String temp = "";
                
                for(int count2 = 0; count2 < 7; count2++)
                    temp += blueBin.charAt(count2);
                temp += "0";
                
                Color col = new Color(pic.getPixelColor(x, y, "red"), pic.getPixelColor(x, y, "green"), bTod(temp), pic.getPixelColor(x, y, "alpha"));
                pic.draw(col.getRGB(), x, y);
                
                ++x;
                if (x ==  dim[0]) {
                    x = 0;
                    ++y;
                }
                count++;
            }
            /* 
             * ==>
             */
            
            Dialog dialog = new Dialog(new javax.swing.JFrame(), true, false);
            
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
            
            //Écriture du fichier
            pic.save(dialog.getPath());
            log("log.txt", "Writing file at: " + dialog.getPath() + "\n");
        }
    }
    
    /* 
     * Décryptage de l'image
     */
    private static void decoding() {
        boolean ok = true;
        int res = 0;
        int[] dim = null;
        String offsetBin = "", text = "", path = "";
        String[] caracBin;
        Picture pic = null;
        
        log("log.txt", "\nD\u00e9cryptage\n");
        do {
            log("log.txt", "Openning dialog box...\n");
            ok = true;
            Dialog dialog = new Dialog(new javax.swing.JFrame(), true, true);
            
            dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                @Override
                public void windowClosing(java.awt.event.WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
            
            //Récupération de l'image
            path = dialog.getPath();
            
            pic = new Picture(path);
            dim = pic.getDimensions();
            res = dim[0] * dim [1];       //Largeur, hauteur
            
            if (res < 16) {
                JOptionPane.showMessageDialog(null, "Image trop petite", "Erreur: taille", JOptionPane.INFORMATION_MESSAGE);
                log("log.txt", "/!\\Error: Image too little!");
                ok = false;
            }
        } while(!ok);
        log("log.txt", "Path: " + path + "\n");
        
        if (!path.equals("")) {
            /* 
             * <==!Expliqué par Franck
             */
            int x = 0, y = 0, count = 0, count2 = 0;
            char temp = ' ';
            
            while(count < 8) {
                offsetBin += dTob(pic.getPixelColor(x, y, "red"), 8).charAt(7);
                
                ++x;
                if (x == dim[0]) {
                    x = 0;
                    ++y;
                }
                count++;
            }
            
            offset = bTod(offsetBin);
            log("log.txt", "Offset: " + offset + "\n");
            /* 
             * ==>
             */
            
            /* 
             * <==!Expliqué par Anthony
             */
            count = (x = (y = 0));
            caracBin = new String[res];
            
            do {
                count = 0;
                while(count < 16) {
                    temp = dTob(pic.getPixelColor(x, y, "blue"), 8).charAt(7);
                    caracBin[count2] += dTob(pic.getPixelColor(x, y, "green"), 8).charAt(7);
                    
                    ++x;
                    if (x == dim[0]) {
                        x = 0;
                        ++y;
                    }
                    count++;
                }
                text += (char) bTod(caracBin[count2]);
                count2++;
            } while(temp == '1');
            
            text = cesar(text, false);
            
            log("log.txt", "Message: " + text);
            JOptionPane.showMessageDialog(null, text, "Message", JOptionPane.INFORMATION_MESSAGE);
            /* 
             * ==>
             */
        }
    }
    
    /* Code César
     * mode = true : décalage classique; mode = false : décalage inverse
     */
    private static String cesar(String text, boolean mode) {
        String text2 = "";
        
        //Génère le décalage aléatoirement entre 1 et 255
        if (mode)
            offset = (int) Math.floor(Math.random() * 254) + 1;
        
        //Applique le décalage
        for(int count = 0; count < text.length(); count++) {
            if (mode)
                text2 += (char) (text.charAt(count) + offset);
            else
                text2 += (char) (text.charAt(count) - offset);
        }
        
        return text2;
    }
    
    
    
    //Récupération du code Unicode décimal du caractère
    private static int unicodeDeci(char carac) {
        return (int) carac;
    }
    
    //Conversion décimal -> binaire
    private static String dTob(int decimal, int size) {
        String temp = "", binary = "";
        int modulo, quotient = 0;
        
        if (decimal <= 1) {
            temp += decimal;
        }
        else {
            while(decimal > 1) {
                quotient = (int) Math.floor(decimal / 2);
                modulo = decimal % 2;
                decimal = quotient;
                temp += modulo;
            }
            temp += quotient;
        }
        
        while(temp.length() < size)
            temp += '0';
        
        //Inversion du sens du mot
        for(int count = temp.length() - 1; count >= 0; count--)
            binary += temp.charAt(count);
        
        return binary;
    }
    
    //Conversion binaire -> décimal
    private static int bTod(String bin) {
        int deci = 0;
        
        for(int count = bin.length() - 1; count >= 0; count--)
            deci += Character.getNumericValue(bin.charAt(count)) * Math.pow(2, bin.length() - count - 1);
        
        return deci;
    }
    
    private static void imageModifier(String path) {
        Picture img = new Picture(path);
        img.save(path);
    }
    
    /* Écrit les données voulues dans un fichier texte
     * Utilisée lors du fonctionnement pour le debug
     */
    private static void log(String path, String text)
    {
        try
        {
            FileWriter fw = new FileWriter(path, true);
            BufferedWriter output = new BufferedWriter(fw);
            
            output.write(text);
            output.flush();
            output.close();
        }
        catch(IOException ioe){
            JOptionPane.showMessageDialog(null, ioe, "Erreur: writing log", JOptionPane.ERROR_MESSAGE);
        }
    }
}