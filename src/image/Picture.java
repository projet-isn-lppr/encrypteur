/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package image;

import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.JOptionPane;
/**
 *
 * @author PC
 */
public class Picture {
    BufferedImage picture = null;
    
    //Constructeur
    public Picture(String path) {
        picture = load(path);
    }
    
    //Dessine le pixel dans la couleur voulue aux coordonnées spécifiées
    public void draw(int rgb, int x, int y) {
        picture.setRGB(x, y, rgb);
    }
    
    /* 
     * Retourne l'octet du sous-pixel demandé
     * Transparence -> "alpha"
     * Rouge -> "red"
     * Vert -> "green"
     * Bleu -> "blue"
     */
    public int getPixelColor(int x, int y, String color) {
        int rgb = picture.getRGB(x, y);
        int col;
        
        switch (color) {
            case("alpha"): col = (rgb >> 24) & 0xFF; break;
            case("red"): col = (rgb >> 16) & 0xFF; break;
            case("green"): col = (rgb >> 8) & 0xFF; break;
            case("blue"): col = rgb & 0xFF; break;
            default: col = -1;
        }
        
        return col;
    }
    
    //Retourne les dimensions de l'image
    public int[] getDimensions() {
        int[] dimensions = {picture.getWidth(), picture.getHeight()};
        return dimensions;
    }
    
    //Charge l'image depuis l'endroit demandé
    private BufferedImage load(String path) {
        try {
            picture = ImageIO.read(new File(path));
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, e, "Erreur: load", JOptionPane.ERROR_MESSAGE);
        }
        
        return picture;
    }
    
    //Écris l'image à l'endroit spécifié
    public void save(String path) {
        File file = new File(path);
        String ext = path.substring(path.lastIndexOf(".") + 1);
        
        try {
            ImageIO.write(picture, ext, file);
        }
        catch(IOException e) {
            JOptionPane.showMessageDialog(null, e, "Erreur: save", JOptionPane.ERROR_MESSAGE);
        }
    }
}